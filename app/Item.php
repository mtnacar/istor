<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function orderItems()
    {
        return $this->hasMany('App\OrderItem');
    }

    public function filter()
    {
        return $this->belongsTo('App\Filter');
    }

    public function price()
    {
        return $this->belongsTo('App\Price');
    }
}
