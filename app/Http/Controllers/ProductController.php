<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use App\Filter;
use App\Price;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        $categories = Category::all();
        $filters = Filter::all();
        $prices = Price::all();


        return view('products.index')->with('items', $items)->with('categories', $categories)->with('filters', $filters)->with('prices', $prices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (\Auth::check()){
            $item = Item::findOrFail($id);
            return view('products.show')->with('item', $item);
        }
        else 
        {
            return redirect()->route('login');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function selectedCategory($id)
    {
        $categories = Category::all();
        $filters = Filter::all();
        $prices = Price::all();
        $items = Item::where('category_id', $id)->get();
        
        return view('products.index')->with('items', $items)->with('categories', $categories)->with('filters', $filters)->with('prices', $prices);
    }

    public function selectedFilter($id)
    {
        $categories = Category::all();
        $filters = Filter::all();
        $prices = Price::all();
        $items = Item::where('filter_id', $id)->get();
        
        return view('products.index')->with('items', $items)->with('categories', $categories)->with('filters', $filters)->with('prices', $prices);
    }

    public function filterPrice($id)
    {
        $categories = Category::all();
        $filters = Filter::all();
        $prices = Price::all();
        $items = Item::where('price_id', $id)->get();
        
        return view('products.index')->with('items', $items)->with('categories', $categories)->with('filters', $filters)->with('prices', $prices);
    }
}
