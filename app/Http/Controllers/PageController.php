<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Category;
use App\Filter;
use App\Price;
use App\Order;
use App\OrderItem;

class PageController extends Controller
{
    public function index()
    {
        $items = Item::inRandomOrder()->limit(4)->get();
        return view('welcome')->with('items', $items);
    }
}
