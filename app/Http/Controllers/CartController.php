<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\OrderItem;
use App\Order;
use App\Item;

class CartController extends Controller
{
    public function add(Request $request, $id){
        $item_id = $id;
        $quantity = intval($request->qty);

        if ($request->session()->exists('cart.'.$item_id)) {
            $currentQuantity = $request->session()->get('cart.'.$item_id);
		    $request->session()->put('cart.'.$item_id, $quantity + $currentQuantity);
        } else {
            $request->session()->put('cart.'.$item_id, $quantity);
            // cart[1]=>10
            // cart.1=>10
        }
    
        Session::flash('success', 'Item(s) added to cart');
	    return redirect()->route('products');
    }

    public function update(Request $request){
        // get product id and quantity 
        $item_id = $request->input('item_id');
        $qty = $request->input('qty');
        // dd($qty);
        // Remove the product 
        $request->session()->forget('cart.'. $item_id);

        // Add the product with the updated quantity
        $request->session()->put('cart.'.$item_id, $qty);

        return redirect()->back();
    }

    public function destroy(Request $request){
        $item_id = $request->input('item_id');
        $request->session()->forget('cart.'. $item_id);
        return redirect()->route('cart.all');
    }

    public function empty(Request $request){
        session()->forget('cart');
        return redirect()->route('cart.all');
    }

    public function index(Request $request){
        $cart = session()->get('cart');
        $item_ids = (!empty($cart)) ? array_keys($cart) : [];
        $items = Item::find($item_ids);

        return view('products.cart', ['items' => $items]);
      }

    public function checkout(Request $request){
        if (\Auth::check()) 
        {
            $cart = session()->get('cart');
  
            $item_ids = array_keys($cart);
    
            $items = Item::find($item_ids);
    
            $order = new Order;
            $order->user_id = auth()->user()->id;
            $order->price = $request->input('totalPrice');
            $order->save();
      
            foreach($items as $item)
            {
                $orderItems = new OrderItem;
                $orderItems->order_id = $order->id;
                $orderItems->item_id = $item->id;
                $orderItems->qty = $cart[$item->id];
                $orderItems->save();
    
                $request->session()->forget('cart.'.$item->id);
            }
    
            Session::flash('success', 'Thank you for shopping!');
            return redirect()->route('home');
        } 
        else 
        {
            return redirect()->route('login');
        }
    }
}