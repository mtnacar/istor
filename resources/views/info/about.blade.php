@extends('layouts.app')

@section('content')

<main>
    <h1 id="about">About Us</h1>

    <p class="about">
        Established in 2020, Sublime Online Sports Shop is a lifestyle brand dedicated to introducing and sustaining a sportswear culture fueled by basketball, skateboarding, people, and most especially, extreme fun to individuals worldwide.
        <br>
        <br>
        This embracement of culture is exchanged through the variety of products Sublime offers, and through the connection it makes with the people.
        <br>
        <br>
        The signature Fire logo is meant to be a fierce reminder for the community to let go, get rid of all inhibitions, and have a Sublime sports lifestyle.
        <br>
        <br>
        Remember to like us on Facebook, or follow us on Instagram and Youtube, to connect with us and be part of the Sublime experience. 
    </p>
</main>

@endsection