@extends('layouts.app')

@section('content')

<div class="wrapper mt-6 mb-0" id="details">
    <div class="col col-12 container">
        <div class="row">
            <div class="col col-12 col-lg-6" id="form">
                <fieldset>
                    <legend id="contact" class="getintouch">
                        <h1 class="ml-3">Need help?</h1>
                    </legend>
                    <div class="contact-form">
                        <form action="mailto:livestrong.matthew@gmail.com" onsubmit="return validateForm()" method="post" required>
                            <div class="details col-12 col-lg-6 offset-lg-2">
                                <label for="name">Full Name:</label><br>
                                <input type="text" name="name" placeholder="Full Name">
                                <br>
                                <label for="email" >E-mail:</label><br>
                                <input type="text" name="mail" placeholder="Email Address">
                                <br>
                                <label for="message" >Message:</label><br>
                                <textarea type="text" name="message" rows="5" cols="45" placeholder="Your Message"></textarea>
                                <br>
                                <br>
                                <button class="btn btn-success">Submit</button>
                                <button class="btn btn-danger" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </fieldset>
            </div>
            
            <div class="wrapper">
                <div class="col col-12" id="map">
                    <div class="card rounded 1" id="card">
                        <div class="card-body">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15456.194300747087!2d121.0257917!3d14.4243634!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x69343e1f3ed366ee!2sIstor%20Data%20Solutions%2C%20Inc.!5e0!3m2!1sen!2sph!4v1597584845750!5m2!1sen!2sph" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="map"></iframe>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>

@endsection