@extends('layouts.app')

@section('content')

    <main>
        <!-- Carousel -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach($items as $key => $item)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($items as $item)
                    <div class="carousel-item {{ $loop->first ? ' active' : '' }}">
                        <img src="{{ $item->img_path }}" alt="{{ $item->name }}">
                    </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- Featured Items -->
        <div class="container">
            <h2 class="wow slideInRight">Featured Items</h2>
            <hr>
            <div class="row text-center wow fadeIn">
            <!-- Retrieve records from products table and display here using class "card" in bootstrap-->
                @foreach($items as $item)
                    <div class="col col-sm-12 col-md-4 col-lg-3 mb-3 d-inline-block">
                        <div class="card h-100 featured">
                            <img src="{{ $item->img_path }}" alt="" class="prodImg">
                            <div class="card-body">
                                <h5 class="card-title">{{ $item->name }}</h5>
                                <h5 class="align-middle">₱ {{ number_format($item->price,2) }}</h5>
                                <a href="{{ route('products.show', ['id' => $item->id]) }}" class="btn buy btn-block btn-primary"><i class="fas fa-shopping-cart"></i> Buy Now </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>

@endsection 