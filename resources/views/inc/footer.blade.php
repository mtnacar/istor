<footer class="footer py-5 bg-dark">
    <div class="container">
        <div class="row">
            <div class="col col-12 col-lg-3 offset-lg-2 mt-3" id="footer-tags">
                <p>Affiliates:</p>
                    <div>
                        <a href="https://www.nike.com/ph/" target="_blank">Nike</a>
                    </div>
                    <div>
                        <a href="https://www.adidas.com.ph/" target="_blank">Adidas</a>
                    </div>
                    <div>
                        <a href="https://www.nike.com/ph/jordan" target="_blank">Jordan</a>
                    </div>
            </div>

            <div class="col col-12 col-lg-3 mt-3" id="footer-tags">
                <p>Follow us:<p>
                    <div>
                        <a href="#" target="_blank">
                            <i class="fab fa-facebook-square fa-2x"></i>
                        </a>
                        <a href="#" target="_blank">
                            <i class="fab fa-youtube fa-2x"></i>
                        </a>
                        <a href="#">
                            <i class="fab fa-instagram fa-2x"></i>
                        </a>
                    </div>
            </div>

            <div class="col col-12 col-lg-3 mt-3" id="footer-tags">
                <p>Sublime Sports</p>
                    <a href="{{ route('about') }}">About Us</a>
                    <br>
                    <a href="{{ route('contact') }}">Contact Us</a>
                    <br>
                    <a href="#">Sublime Privacy Policy</a>
            </div>
        </div>

        <div class="row">
            <div class="col col-lg-1 offset-lg-5">
                <p>
                    <div class="fb-like" data-href="http://127.0.0.1:8000/" data-width="" data-layout="button" data-action="like" data-size="large" data-share="false"></div>
                </p>
            </div>

            <div class="col">
                <p>
                    <div class="fb-share-button mx-auto" data-href="http://127.0.0.1:8000/" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2F127.0.0.1%3A8000%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                        Share</a>
                    </div>
                </p>
            </div> 
        </div>

        <div class="row">
            <div class="col mx-auto">
                <nav class="navbar navbar-expand-lg text-primary mb-2">
                    <p class="navbar-nav mx-auto">&copy; 2020 Sublime Online Sports Shop. All rights reserved.</p>
                </nav>
            </div>
        </div>
    </div>
</footer>