@extends('layouts.app')

@section('content')

    <main>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 mt-5">
                    <!-- <img src="{{ $item->img_path }}" alt="" style="width: 100%; height: 250px; border-bottom: 1px solid #ddd;"> -->
                    <div class="container">
                        <div class="mySlides">
                            <div class="numbertext">1 / 3</div>
                            <img src="{{ $item->img_path }}" style="width:100%">
                        </div>

                        <div class="mySlides">
                            <div class="numbertext">2 / 3</div>
                            <img src="{{ $item->img_pathTwo }}" style="width:100%">
                        </div>

                        <div class="mySlides">
                            <div class="numbertext">3 / 3</div>
                            <img src="{{ $item->img_pathThree }}" style="width:100%">
                        </div>
                            
                            
                        <a class="prev" onclick="plusSlides(-1)">❮</a>
                        <a class="next" onclick="plusSlides(1)">❯</a>

                        <div class="caption-container">
                            <p id="caption"></p>
                        </div>

                        <div class="row">
                            <div class="column">
                            <img class="demo cursor" src="{{ $item->img_path }}" style="width:100%" onclick="currentSlide(1)">
                            </div>
                            <div class="column">
                            <img class="demo cursor" src="{{ $item->img_pathTwo }}" style="width:100%" onclick="currentSlide(2)">
                            </div>
                            <div class="column">
                            <img class="demo cursor" src="{{ $item->img_pathThree }}" style="width:100%" onclick="currentSlide(3)">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <h2 class="mt-5">PRODUCT INFORMATION</h2>
                    <hr>
                    <h4>{{ $item->name }}</h4>
                    <p class="desc text-justify">{{ $item->description }}</p>
                    <h5>₱ {{ number_format($item->price,2) }}</h5>
                    <form method="POST" action="{{ route('cart.add', ['id' => $item->id]) }}">
                        @csrf
                        <div class="row">
                            <div class="col d-flex d-inline">
                                <input type="number" class="form-control col-lg-1 p-3" min="1" max="10" value="1" name="qty" id="qty">
                                <button class="btn btn-block btn-primary col-lg-3 ml-2" type="submit"><i class="fas fa-shopping-cart"></i> Add to Cart </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

@endsection