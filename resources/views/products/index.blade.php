@extends('layouts.app')

@section('content')
    
    <main>
        <div class="container collections">
            <div class="row cat">
                <!-- Catalogue -->
                <div class="col col-sm-12 col-lg-2">
                    <h4>Collection</h4>
                    <div class="list-group">
                        @foreach ($categories as $category)
                        <a href="{{ route('selectedCategory', ['id' => $category->id]) }}" class="list-group-item list-group-item-action"><i class="fas fa-chevron-circle-right"></i> {{ $category->name }}</a>
                        @endforeach
                        <div id="sort">Sort by:</div>

                        <div class="dropdown">
                            <button onclick="myFunction()" class="dropbtn brand">Brand</button>
                            
                            <div id="myDropdown" class="dropdown-content">
                            @foreach ($filters as $filter)
                            <a href="{{ route('selectedFilter', ['id' => $filter->id]) }}" class="list-group-item list-group-item-action"><i class="fas fa-chevron-circle-right"></i> {{ $filter->name }}</a>
                            @endforeach
                            </div>

                            <button onclick="otherFunction()" class="dropbtn price">Price</button>
                            <div id="otherDropdown" class="dropdown-content">
                            @foreach ($prices as $price)
                            <a href="{{ route('filterPrice', ['id' => $price->id]) }}" class="list-group-item list-group-item-action"><i class="fas fa-chevron-circle-right"></i> {{ $price->price_range }}</a>
                            @endforeach
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Search Box -->
                <div class="col col-sm-12 col-md-6 col-lg-10">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <input type="text" class="col-lg-12 form-control" id="search">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            @foreach ($items as $item)
                                <div class="col col-lg-4 mb-3 d-inline-block">
                                    <div class="card h-100">
                                        <img src="{{ $item->img_path }}" alt="" class="prodImg">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $item->name }}</h5>
                                            <h5 class="align-left">₱ {{ number_format($item->price, 2) }}</h5>
                                            <a href="{{ route('products.show', ['id' => $item->id]) }}" class="btn btn-block btn-primary"><i class="fas fa-shopping-cart"></i> Buy Now </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection