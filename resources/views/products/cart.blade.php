@extends('layouts.app')

@section('title', 'cart')
<main>

@section('cart-rows')
    <div class="container">

        @php
            $total = 0;
        @endphp

        @foreach ($items as $item)

            @php
                $subtotal = $item->price * session('cart.'.$item->id);
                $total += $subtotal;
            @endphp
            <br>

            <tr>
                <td>{{ $item->name }}</td>
                <td class='text-right'>₱{{ number_format($item->price, 2) }}</td>
                <td>
                    {{-- Update Cart --}}
                    <form method='post' action='{{ route("cart.update", ["id" => $item->id]) }}'>
                        @csrf
                        @method('PATCH')
                        <input type='hidden' name='item_id' value='{{ $item->id }}'>
                        <div class='btn-group btn-block'>
                            <input type='number' name='qty' value='{{ session("cart.".$item->id) }}' class='form-control' required>
                            <button class='btn btn-success' type='submit'>Update</button>
                        </div>
                    </form>

                </td>
                <td class='text-right'>₱{{ number_format($subtotal, 2) }}</td>
                <td>
                    {{-- Delete --}}
                    <form method='post' action='{{ route("cart.destroy", ["id" => $item->id]) }}'>
                        @csrf
                        @method('DELETE')
                        <input type='hidden' name='item_id' value='{{ $item->id }}'>
                        <button class='btn btn-danger'>Remove</button>
                    </form>

                </td>
            </tr>

        @endforeach

    @endsection

    @section('content')

        <div class="container">

            @if (!empty(session('cart')))

                <h3>Cart</h3>

                    <div class="row">
                        <table class="table cart table-bordered col-lg-6">

                            <thead>

                                <tr>
                                    <th>Item</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Subtotal</th>
                                    <th>Action</th>
                                </tr>

                            </thead>

                            <tbody>
                                
                                @yield('cart-rows')

                                

                            </tbody>

                        </table>

                        <div class="col-lg-4">

                                <tr class="table cart table-bordered">
                                    <td colspan="3" class="text-right"><h1>Total:</h1></td>
                                    <br>
                                    <td class="text-right"><h2>₱ {{ number_format($total, 2) }}<h2></td>
                                    <td></td>
                                </tr>
                                <hr>


                            {{-- Empty Cart --}}
                            <form action="{{ route('cart.empty') }}" action="d-inline" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger w-100">Empty Cart</button>
                            </form>

                            {{-- Checkout --}}
                            <form action="/cart/checkout/cash" class="d-inline" method="POST">
                            @csrf
                            <input type="hidden" name="total" value="{{$total}}">
                            <button class="btn btn-primary mt-1 w-100">Proceed to Checkout</button>
                            </form>
                            <br>

                            <form action="/cart/checkout/paypal" hidden id="form-checkout-paypal" method="POST">
                            @csrf
                            <input type="hidden" name="total" value="{{$total}}">
                            </form>

                            <div id="paypal-button-container" class="paypal mt-1"></div>
                            <br>
                        </div>
                        
                    </div>
                    


            @else
                <div class="empty">
                    <div class="row mt-5">
                        <div class="col">
                            <div class="text-center">
                                <h3>No items in cart.</h3>

                                <p style="color: black">Select a menu item to add to cart from <a class="here" href="{{ route('products') }}">here.</a></p>
                            </div>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </div>

</main>

<script>

const btnPaypalStyle = {
    height: 38,
    shape: 'rect',
    layout: 'horizontal'
};

paypal.Buttons({ 
    style: btnPaypalStyle,
    createOrder: function(data, actions) {
        // Sets up transaction details including amount and item details.
        return actions.order.create({
            purchase_units: [{
                amount: {
                    value: '{{ $total }}'
                }
            }]
        })
    },
    onApprove: function(data, actions) {
        return actions.order.capture().then(function(details) {
            // This function shows a transaction success message to your buyer.
            alert('Transaction completed by ' + details.payer.name.given_name);
            document.querySelector('#form-checkout-paypal').submit();
        })
    }
}).render('#paypal-button-container');

</script>

@endsection