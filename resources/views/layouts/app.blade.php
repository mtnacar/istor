<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Meta -->
    <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="author" content="Matthew T. Nacar">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Title -->
        <title>Sublime &#149 Online Store</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/sublimelogo.jpg') }}">

        <!-- Fontawesome -->
        <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>

        <!-- Bootswatch CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <!-- Toastr CSS -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/toastr.min.css')}}">

        <!-- Animate CSS-->
        <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

        <!-- Wow JS -->
        <script src="{{ asset('js/wow.js') }}"></script>

        <!-- Boostrap JS
        <script src="{{ asset('js/app.js') }}" defer></script> -->

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

</head>
<body>

<script src="https://www.paypal.com/sdk/js?client-id=AZNML0wMPB9RPC9HgnTTlE4tAmMgu7CpNDRj2xa29cAtX0hTto6pmpE-lrGdcOSaFBCsc9a6mZE7tq19&currency=PHP"></script>

    <div id="app">
        @include('inc.navbar')
        @yield('content')
        @include('inc.footer')
    </div>
            

        <!-- jQuery -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}" defer></script>
        <!-- Popper -->
        <script src="{{ asset('js/popper.min.js') }}" defer></script>
        <!-- Bootstrap -->
        <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
        <!-- Custom JS -->
        <script src="{{ asset('js/script.js') }}"></script>
        <!-- Toastr JS -->
        <script src="{{ asset('js/toastr.min.js') }}"></script>
       
        <script type="text/javascript">
            @if(Session::has('success'))
                toastr.success(" {{ Session::get('success') }} ")
            @endif
        </script>

        <script>
        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
        showSlides(slideIndex += n);
        }

        function currentSlide(n) {
        showSlides(slideIndex = n);
        }

        function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
        }
        </script>

        <script>
            // Create Filters
            $('.filter').on('click',function(){
            var $q = $(this).attr('data-filter');
            if($(this).hasClass('active')){
                low.filter();
                $('.filter').removeClass('active');
            } else {
                low.filter(function(item) {
                return (item.values().date == $q);
                });
                $('.filter').removeClass('active');
                $(this).addClass('active');
            }
            });

            // Return # of items
            var $count = $('.count')
            $count.append(low.size());
            low.on('filterComplete', function(){
            $count.text(low.update().matchingItems.length);
            });
        </script>

        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0" nonce="3wdfssSv"></script>
        
        <script>
        /* When the user clicks on the button, 
        toggle between hiding and showing the dropdown content */
        function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
        }

        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
            }
        }
        }
        </script>

<script>
        /* When the user clicks on the button, 
        toggle between hiding and showing the dropdown content */
        function otherFunction() {
        document.getElementById("otherDropdown").classList.toggle("show");
        }

        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
            }
        }
        }
        </script>

</body>
</html>
