<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PageController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/aboutus', 'AboutController@index')->name('about');
Route::get('/contactus', 'ContactController@index')->name('contact');

Route::post('/cart/add/{id}', 'CartController@add')->name('cart.add');
Route::get('/cart', 'CartController@index')->name('cart.all');
Route::delete('/cart/empty', 'CartController@empty')->name('cart.empty');
Route::delete('/cart/destroy', 'CartController@destroy')->name('cart.destroy');
Route::patch('/cart/update', 'CartController@update')->name('cart.update');
Route::post('/cart/checkout', 'CartController@checkout')->name('cart.checkout');

Route::get('/products', 'ProductController@index')->name('products');
Route::get('/selectedCategory/{id}', 'ProductController@selectedCategory')->name('selectedCategory');
Route::get('/selectedFilter/{id}', 'ProductController@selectedFilter')->name('selectedFilter');
Route::get('/filterPrice/{id}', 'ProductController@filterPrice')->name('filterPrice');
Route::get('/products/show/{id}', 'ProductController@show')->name('products.show');