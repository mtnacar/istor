<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert(
            [
                [ 
                    'price_range' => '₱900 - ₱4,000',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'price_range' => '₱4,001 - ₱9,000',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ]
            ]
        );
    }
}
