<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [ 
                    'name' => 'Headwear',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Apparel',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Footwear',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ], 
                [ 
                    'name' => 'Accessories',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ]
            ]
        );
    }
}
