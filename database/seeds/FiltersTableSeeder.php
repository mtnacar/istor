<?php

use Illuminate\Database\Seeder;

class FiltersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('filters')->insert(
            [
                [ 
                    'name' => 'Nike',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Adidas',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Jordan',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ]
            ]
        );
    }
}
