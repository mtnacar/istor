<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert(
            [
                [ 
                    'name' => 'Nike Snapback',
                    'price' => '1045.00',
                    'description' => 'The Nike Sportswear Pro Adjustable Hat features a classic six-panel design with a snap-back closure for custom comfort.',
                    'img_path' => 'https://i3.stycdn.net/images/2013/12/52/article/nike/ca5502402/nike-true-snapback-cap-blau-weiss-130-zoom-0.jpg',
                    'img_pathTwo' => 'https://images-na.ssl-images-amazon.com/images/I/81FCNGCpaeL._AC_UL1500_.jpg',
                    'img_pathThree' => 'https://media.sssports.com/630x630/media/catalog/product/1/9/193154226027_2.jpg',
                    'category_id' => '1',
                    'filter_id' => '1',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(),

                ],
                [ 
                    'name' => 'Adidas Snapback',
                    'price' => '910.00',
                    'description' => "This flat-brim cap flashes true adidas Originals style with a big embroidered Trefoil on the front. It's made for comfort, with a stretch-cotton build and an adjustable snapback closure.",
                    'img_path' => 'https://picture-cdn.wheretoget.it/5iztwk-i.jpg',
                    'img_pathTwo' => 'https://images.journeys.com/images/products/1_426893_ZM_BLACK.JPG',
                    'img_pathThree' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSUb18FqFhlCS4zW0rh_-iEUhvG6NZvBJz2vw&usqp=CAU',
                    'category_id' => '1',
                    'filter_id' => '2',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Nike Crew SSNL',
                    'price' => '1395.00',
                    'description' => 'The Nike Sportswear T-Shirt stacks throwback graphics on soft cotton for a comfortable standout look.',
                    'img_path' => 'https://ae01.alicdn.com/kf/HTB1Nfp.beP2gK0jSZFoq6yuIVXae/Original-New-Arrival-NIKE-AS-M-NSW-SS-CREW-SSNL-3-Men-s-T-shirts-short.jpg',
                    'img_pathTwo' => 'https://ae01.alicdn.com/kf/He570dbd589564f888ac06fdc1a4695a7X/Original-New-Arrival-NIKE-AS-M-NSW-SS-CREW-SSNL-3-Men-s-T-shirts-short.jpg_q50.jpg',
                    'img_pathThree' => 'https://ae01.alicdn.com/kf/H5c6ccdc5e0d646d4a5ab8a07fb4353f1l.jpg',
                    'category_id' => '2',
                    'filter_id' => '1',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Adidas Trefoil',
                    'price' => '1300.00',
                    'description' => "The adidas Trefoil first appeared on the scene in 1972, and it continues to make its mark. This archive-inspired t-shirt celebrates the symbol's legacy. It's made of cotton jersey for a soft feel.",
                    'img_path' => 'https://5.imimg.com/data5/SV/XT/MY-55472971/black-t-shirt-500x500.png',
                    'img_pathTwo' => 'https://cdn.shopify.com/s/files/1/0017/4587/9106/products/DV2905AA.jpg?v=1570860240',
                    'img_pathThree' => 'https://www.stuartslondon.com/images/adidas-originals-womens-boyfriend-trefoil-t-shirt-black-p42908-451858_image.jpg',
                    'category_id' => '2',
                    'filter_id' => '2',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Nike Lebron 17 Titan',
                    'price' => '8095.00',
                    'description' => "Fine-tuned for LeBron's ferocious game, the LeBron 17 Titan has a sleek, low-top design. It uses a powerful combination of impact-ready cushioning and smooth, responsive Nike React foam.",
                    'img_path' => 'https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2020%2F07%2Ftitan-nike-lebron-james-17-low-official-release-date-info-4.jpg?q=75&w=800&cbr=1&fit=max',
                    'img_pathTwo' => 'https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2020%2F07%2Ftitan-nike-lebron-james-17-low-official-release-date-info-3.jpg?w=1600&cbr=1&q=90&fit=max',
                    'img_pathThree' => 'https://static.titan22.com/media/wysiwyg/Blog/blog-1.jpg',
                    'category_id' => '3',
                    'filter_id' => '1',
                    'price_id' => '2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Jordan Spizike GS White Cement',
                    'price' => '8304.75',
                    'description' => 'This grade-school edition of the Jordan Spizike ‘White Cement’ applies a classic color scheme to a hybrid silhouette built with complementary parts from legacy Jordan silhouettes. The mid-top sports a white leather upper with speckled grey ‘wings’ and modified elephant-print overlays on the toe and heel. Contrasting pops of red appear on the shoe’s collar lining and branding elements.',
                    'img_path' => 'https://stockx-sneaker-analysis.s3.amazonaws.com/wp-content/uploads/2017/06/Jordan-Spizike-White-Cement-2017.png',
                    'img_pathTwo' => 'https://cdn.weartesters.com/wp-content/uploads/2017/06/Jordan-Spizike-White-Cement-5.jpg',
                    'img_pathThree' => 'https://sneakerbardetroit.com/wp-content/uploads/2017/05/jordan-spizike-white-cement-release-date-315371-122-02.jpg',
                    'category_id' => '3',
                    'filter_id' => '3',
                    'price_id' => '2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Adidas NMD R1 V2',
                    'price' => '7500.00',
                    'description' => "Shoes are the next generation of a line that's in step with your ambitions. Just like the originals, they have a soft textile upper that hugs your foot and keeps your stride cool. The outsole is cushioned to return energy with every step you take. The colours are bold and fresh. So keep moving fearlessly.",
                    'img_path' => 'https://gcs.justfreshkicks.com/2020/02/9f1b14f2-45fee9b8-8395-43df-ad5c-8625ce9b6f6d.png',
                    'img_pathTwo' => 'https://gcs.justfreshkicks.com/2020/02/a459a95e-f11f8d1f-0f8f-4330-ae1c-219fb1a8fa35.png',
                    'img_pathThree' => 'https://gcs.justfreshkicks.com/2020/02/14b31fe7-72dc582f-d012-498b-91fc-5f1cf9fb8040.png',
                    'category_id' => '3',
                    'filter_id' => '2',
                    'price_id' => '2',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Adidas 3MC',
                    'price' => '3300.00',
                    'description' => 'Combining skate-friendly design with a clean, classic profile, these shoes are in their element on or off the board. Their highly versatile style is rider-friendly, with a reinforced canvas upper and an ultra-flexible Geoflex outsole that moves naturally with your foot and has great board feel.',
                    'img_path' => 'https://photos6.spartoo.eu/photos/774/7742934/7742934_500_A.jpg',
                    'img_pathTwo' => 'https://www.efootwear.eu/media/catalog/product/cache/image/650x650/0/0/0000200458748_03_yt.jpg',
                    'img_pathThree' => 'https://images.asos-media.com/products/adidas-skateboarding-3mc-trainers-in-black-b22706/9419507-1-black?$XXL$&wid=513&fit=constrain',
                    'category_id' => '3',
                    'filter_id' => '2',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Nike Brasilia Duffel Bag',
                    'price' => '2795.00',
                    'description' => 'The durable Nike Brasilia Duffel features a spacious main compartment for all your gear so you can feel prepared. Padded shoulder strap makes carrying comfortable, and multiple exterior pockets provide quick-grab convenience.',
                    'img_path' => 'https://image.keller-sports.com/storage/products/4B/AA/4BAAA8439AE9F8D8040306A8866A59F7BAd0.1000x1000.jpeg',
                    'img_pathTwo' => 'https://www.traininn.com/f/13719/137193635/nike-brasilia-duffle-l-9.0-95l.jpg',
                    'img_pathThree' => 'https://cdn.shopify.com/s/files/1/0835/5343/products/BA5976-010a.png?v=1551312187',
                    'category_id' => '4',
                    'filter_id' => '1',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
                [ 
                    'name' => 'Adidas 4ATHLTS Backpack',
                    'price' => '2100.00',
                    'description' => "With the right mindset, even the city is a playing field. Team up with the adidas 4ATHLTS Backpack and take it all on. Stow everything you need for a full day's adventure in a roomy build that keeps the carry comfortable. Laptop optional.",
                    'img_path' => 'https://assets.adidas.com/images/w_600,f_auto,q_auto/a80c914b3e17440a8f2aaaf600be7f22_9366/4ATHLTS_Backpack_Black_FJ4441_01_standard.jpg',
                    'img_pathTwo' => 'https://media.sssports.com/630x630/media/catalog/product/4/0/4062054585571_3.jpg',
                    'img_pathThree' => 'https://cf.shopee.co.id/file/f430d3a81e4aaacddd0ecc08dc10a8dc',
                    'category_id' => '4',
                    'filter_id' => '2',
                    'price_id' => '1',
                    'created_at' => now(), 
                    'updated_at' => now(), 
                ],
            ]
        );
    }
}
