$(document).ready(function(){

    function wowInit() {
        new WOW().init();
    }
    wowInit();

    //prevent both backspace and delete keys in input field (qty)
    $("#qty").keypress(function (e) {
        e.preventDefault();
    }).keydown(function(e){
        if ( e.keyCode === 8 || e.keyCode === 46 ) {
            return false;
        }
    });

});
